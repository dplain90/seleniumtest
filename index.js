const express = require('express')
const app = express()
const {Builder, By, Key, Origin, until, input, Capabilities } = require('selenium-webdriver');
app.get('/', (req, res) => res.send("<div id='test'>Hello World!</div>"))

app.listen(3000)


const runDriver = async () => {
  let driver = await new Builder().forBrowser('firefox')
    .usingServer('http://localhost:4444/wd/hub')
    .build();

  await driver.get('http://localhost:3000/');

  const div = await driver.findElement(By.id('test'))
  console.log('heres div', div)

}

try {
  console.log('running driver...')
  runDriver()


} catch (e) {

  console.log('heres errorr', e)
}
